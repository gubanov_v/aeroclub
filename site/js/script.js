$(document).ready(function () {
    
    
    $('.main-slider ').owlCarousel({
      items: 1,
      loop: true,
      margin: 0,
      nav: true,
      dots: true,
//      dotsContainer: "class",
      navClass: ["nav nav-large nav-left-white", "nav nav-large nav-right-white"],
        responsive:{
        0:{
            nav:false,
        },
        960:{
            nav:true,
            
        }
        }

    });
    $('.mini-slider').owlCarousel({
      items: 4,
      loop: true,
      margin: 10,
      nav: true,
      dots: false,
      autoWidth: true,

      navClass: ["nav nav-large nav-left-red", "nav nav-large nav-right-red"],
         responsive:{
        0:{
            items:1,
            nav:false,
            autoplay: true
        },
        480:{
            items:2,
            autoWidth: false,
            center: false, 
            nav: false,
            autoplay: true
            
        },
        768:{
            items:2,
            autoWidth: false,
            center: false, 
            nav: false,
            autoplay: false
        },

        960:{
            items:1,
            autoWidth: false,
            loop: true,
            center: true, 
            nav: true,
            autoplay: false
        }
    }
    });
    $('.footer-slider ').owlCarousel({
      items: 3,
      loop: false,
      margin: 30,
      nav: true,
      dots: false,
        autoWidth: false,
      center: false,
      navClass: ["nav nav-small small-left", "nav nav-small small-right"],
        responsive:{
        0:{
            items:1
            
        },
            768:{
            items:3
        },
            960: {
            items: 2
        },
            1200: {
            items: 3
        }
    }
    });
  
      $('.service-slider').owlCarousel({
      items: 3,
      loop: true,
      margin: 1,
      nav: true,
      dots: false,
          center: true,
      navClass: ["nav nav-medium medium-left", "nav nav-medium medium-right"],
          responsive:{
        0:{
            items:1,
            nav:false,
            autoplay: true
        },
        480:{
            items:1,
            autoWidth: false,
            center: false, 
            nav: true,
            autoplay: true
            
        },
        768:{
            items:2,
            autoWidth: false,
            center: false, 
            nav: true,
            autoplay: false
        },
        1200:{
             items:3,
        }
    }
    });
      
$('.portfolio-slider').owlCarousel({
      items: 2,
      loop: true,
      margin: 1,
      nav: true,
      dots: false,
      center: false,

      navClass: ["nav nav-medium medium-left", "nav nav-medium medium-right"],
          responsive:{
        0:{
            items:1,
            nav:false,
            autoplay: true
        },
        480:{
            items:1,
            autoWidth: false,
            center: false, 
            nav: true,
            autoplay: true
            
        },
        768:{
            items:2,
            autoWidth: false,
            center: false, 
            nav: true,
            autoplay: false
        },
        1200:{
             items:2,
        }
    }
    });
    $('.cases-slider').owlCarousel({
      items: 3,
      loop: true,
      margin: 1,
      nav: true,
      dots: false,
        center: true,
      navClass: ["nav nav-small  nav-left-red", "nav nav-small nav-right-red"],
          responsive:{
        0:{
            items:1,
            nav:false,
            autoplay: true
        },
        480:{
            items:1,
            autoWidth: false,
            center: false, 
            nav: true,
            autoplay: true
            
        },
        768:{
            items:2,
            autoWidth: false,
            center: false, 
            nav: true,
            autoplay: false
        },
        1200:{
             items:3,
            nav: true,
            merge: true,
//            autoWidth: true,
//             center: true,
        }
    }
    });
  $('.choose-slider').owlCarousel({
      items: 4,
      margin: -1,
      nav: true,
      dots: false,
      autoWidth: false,
      center: false,
      merge: true,
      navClass: ["nav nav-medium medium-left", "nav nav-medium medium-right"],
      responsive:{
        0:{
            items:1,
            nav:true,
            autoplay: true
        },
        480:{
            items:1,
            nav: true,
            autoplay: true
            
        },
        768:{
            items:3,

            nav: true,
            autoplay: false
        }
    }
    });
    
      $('.anchor-slider').owlCarousel({
      items: 3,
      margin: -1,
      nav: true,
      dots: false,
      autoWidth: false,
      center: false,
      merge: true,
          mouseDrag: false,
      navClass: ["nav nav-medium medium-left", "nav nav-medium medium-right"],
      responsive:{
        0:{
            items:0,
            nav:true,
            autoplay: true,

        },
        480:{
            items:3,
            nav: true,
            autoplay: true
            
        },
        768:{
            items:3,

            nav: true,
            autoplay: false
        }
    }
    });
  
  $('.team-slider').owlCarousel({
      items: 4,
        loop: true,
      margin: 100,
      nav: true,
      dots: false,
      autoWidth: false,
      center: false,
      navClass: ["nav nav-medium medium-left-red", "nav nav-medium medium-right-red"],
      responsive:{
        0:{
            items:1,
            nav:false,
            autoplay: true
        },
        480:{
            items:2,
            autoWidth: false,
            center: false, 
            nav: false,
            autoplay: true
            
        },
        768:{
            items:3,
            autoWidth: false,
            center: false, 
            nav: false,
            autoplay: true
        },
           815:{
            items:3,
            autoWidth: false,
            center: false, 
            nav: true,
            autoplay: false
        },
          960: {
              items:4,
            autoWidth: false,
            center: false, 
            nav: true,
            autoplay: false,
    
              margin: 10
          }
    }
    });
  
    $('.office-slider-inner').owlCarousel({
      items: 7,
      loop: false,
      margin: 30,
      nav: true,
      dots: false,
      autoWidth: true,
      center: false,
      navClass: ["nav nav-medium medium-left-red", "nav nav-medium medium-right-red"],
        responsive:{
        0:{
            items:1,
            nav:false,
            autoplay: true
        },
        480:{
            items:1,
            autoWidth: false,
            center: false, 
            nav: true,
            autoplay: true
            
        },
        768:{
            items:2,
            autoWidth: false,
            center: false, 
            nav: true,
            autoplay: false
        },
        1200:{
             items:3,
        }
    }
    });
  
    $('.diplomas-slider').owlCarousel({
      items: 3,
    loop: true,
      margin: 20,
      nav: true,
      dots: false,
      autoWidth: false,
      center: false,
      navClass: ["nav nav-medium medium-left-red", "nav nav-medium medium-right-red"],
        responsive:{
        0:{
            items:1,
            nav:false,
            autoplay: true,
            center: true
        },
        480:{
            items:2,
        },

        768:{
            items:3,
            autoWidth: false,
            center: false, 
            nav: false,
            autoplay: true
        },
            815:{
            items:3,
            autoWidth: false,
            center: false, 
            nav: true,
            autoplay: false
        },
            960:{
                 items:2,
            },
            1480: {
                items:3,
            }
    }
    });
  
  $('.clients-slider').owlCarousel({
      items: 3,
    loop: true,
      margin: 0,
      nav: true,
      dots: false,
      autoWidth: false,
      center: false,
      navClass: ["nav nav-medium medium-left-red", "nav nav-medium medium-right-red"],
      responsive:{
        0:{
            items:2,
            nav:false,
            autoplay: true,

        },
        480:{
            items:2,
            
        },
768:{
            items:3,
            autoWidth: false,
            center: false, 
            nav: false,
            autoplay: true
        },
            815:{
            items:3,
            autoWidth: false,
            center: false, 
            nav: true,
            autoplay: false
        },
            960:{
                 items:2,
            },
          1480: {
                items:3,
            }
    }
    });
    
    $('.video-slider').owlCarousel({
      items: 3,
      loop: true,
      margin: 100,
      nav: true,
      dots: false,
      autoWidth: false,
      autoHeight: true,
      center: false,
      navClass: ["nav nav-medium medium-left-red", "nav nav-medium medium-right-red"],
        responsive:{
        0:{
            items:1,
            nav:false,
            autoplay: false,

        },
        480:{
            items:1,
            nav:true,
            
        },
        768:{
            items:2,
            autoWidth: false,
            center: false, 
            nav: true,
            autoplay: false
        },
           
            960:{
                 items:3,
            }
    }
    });

  
    $('.office-slider').owlCarousel({
      items: 1,
      loop: true,
      margin: 0,
      nav: true,
      dots: false,
      URLhashListener: true,
      autoWidth: false,
      center: true,
      callbacks: true,
//      startPosition: 'URLHash',
      navClass: ["nav nav-medium medium-left-red", "nav nav-medium medium-right-red"]
    });
  
//    var float_top = $("header").height();
          //  var is_fixed = true;
    
//    $(window).scroll(function(){ 
//        var top = $(this).scrollTop(); 
//        
//        if (top>float_top)    {   
//            //if (is_fixed){
//            //$clone =  $("header").clone();
//            //$clone.hide().prependTo("body").addClass("menu-fixed").slideDown(); 
//             // is_fixed=false;
//            //}
//            $("header").hide().addClass("menu-fixed").slideDown();
//            $("body").css({"padding-top" : float_top});
//            }
//        else {
//            //$("header.menu-fixed").slideUp(function(){$(this).remove()}); 
//            $("header").removeClass("menu-fixed");
//              $("body").css({"padding-top" : 0});
//            //is_fixed=true;
//}
//   
//  });
    
    
     
        // Toggle button
        $('.toggle-button ').click(function () {
            $('.page-content').toggleClass('shift-left')
            $('.menu-mobile').toggleClass('shift-right')
        });
        $('.menu-mobile .close ').click(function () {
            $('.page-content').toggleClass('shift-left')
            $('.menu-mobile').toggleClass('shift-right')
        });


});