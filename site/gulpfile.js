var gulp = require('gulp'),
//    sass = require('gulp-sass'),
//    inky = require('inky'),
//    inlineCss = require('gulp-inline-css');
//    strip = require('gulp-strip-comments');
    uncss = require('gulp-uncss');


//STYLES
gulp.task('styles', function () {
  return gulp.src('./scss/styles.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist'));
});

gulp.task('uncss', function () {
  return gulp.src('./style.css')
        .pipe(uncss({
            html: '*.html'
        }))
        .pipe(gulp.dest('./out'));
});

//CONVERTE INKY
gulp.task('inky', function() {
  return gulp.src('./templates/**/*.html')
    .pipe(inky())
    .pipe(gulp.dest('./dist'));
});

//INLINE CSS
gulp.task('inline', function () {
  return gulp.src('./dist/*.html')
        .pipe(inlineCss())
        .pipe(gulp.dest('./dist/inlined'));
});

 //REMOVE COMMETNS
gulp.task('rc', function () {
  return gulp.src('./dist/*.html')
    .pipe(strip())
    .pipe(gulp.dest('./dist/no_commetns'));
});


//WATCH
gulp.task('default',function() {
    gulp.watch('./scss/**/*.scss',['styles']);
    gulp.watch('./dist/*.html',['inline']);
    gulp.watch('./templates/**/*.html',['inky']);
});

